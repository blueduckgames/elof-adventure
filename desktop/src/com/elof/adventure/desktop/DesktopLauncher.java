package com.elof.adventure.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.elof.adventure.elof_Adventure;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
                config.title = "Elof's Adventure - Alpha 0.1";
                config.backgroundFPS = 60;
                config.y = 0;
                config.vSyncEnabled =true;
                config.width= 1024;
                config.height= 576;
		new LwjglApplication(new elof_Adventure(), config);
	}
}
