package com.elof.adventure;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class elof_Adventure extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
        
        private TiledMap map;
        // Skirtas renderint mapą
	private OrthogonalTiledMapRenderer tiledMapRenderer;
        private OrthographicCamera camera;
        private Player player;
        
        
        
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
                // Map loaderis
                TmxMapLoader loader = new TmxMapLoader();
                map = loader.load("maps/test.tmx");
                tiledMapRenderer = new OrthogonalTiledMapRenderer(map);
                camera = new OrthographicCamera();
                camera.setToOrtho(false,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
                
                // Kuriam playeri
                player = new Player(new Sprite(new Texture("player.png")), (TiledMapTileLayer) map.getLayers().get(0) ); 
                player.setPosition(150, 150);
                
	}

	@Override
	public void render () {
		//Gdx.gl.glClearColor(1, 1, 0, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//batch.begin();
		//batch.draw(img, 0, 0);
		//batch.end();
                tiledMapRenderer.setView(camera);
                tiledMapRenderer.render();
                
                // Įdeda playerį į renderinimą
                tiledMapRenderer.getBatch().begin();
                player.draw(tiledMapRenderer.getBatch());
                tiledMapRenderer.getBatch().end();
                
	}
        
        @Override
        public void resize(int width, int height){
            camera.viewportHeight = height;
            camera.viewportWidth = width;
            camera.update();
        }
        
        
	@Override
	public void pause () {
	}

	@Override
	public void resume () {
	}

 
	@Override
        // DISPOSE, kad nebūtų memory leak
	public void dispose () {
            map.dispose();
            tiledMapRenderer.dispose();
            player.getTexture().dispose();
	}
}
