/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elof.adventure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

/**
 *
 * @author newtr
 */
class Player extends Sprite{
    
    //Vector 2 laiko x ir y skaicius 0, 0
    private Vector2 movement = new Vector2();
    
    // Kaip greitai vaikscioja 
    private float speed = 60*2;
    
    // Gravitacija // 1.0f - pixels per second
    private float gravity = 60*1.0f;
    
    // Collisionams
    TiledMapTileLayer collisionLayer;
    
    // Konsturktorius kuriant player 
    // sprite - kaip atrodys žaidėjas
    public Player(Sprite sprite, TiledMapTileLayer collisionLayer){
        super(sprite);
        this.collisionLayer = collisionLayer;
    }
    
    // Piešia playerį
    @Override
  	public void draw (Batch batch) {
            update(Gdx.graphics.getDeltaTime()); // ima pagal frames ir atnaujina jį
            super.draw(batch);
	}

    private void update(float deltaTime) {
        // Įjungia gravitaciją, kad trauktų žemyn yra minusas
       movement.y -= gravity * deltaTime;
       
       // Apribojam, kad neatsirastų begalybės ten.
       if (movement.y > speed){
           movement.y = speed;
       } else if(movement.y < -speed) {
           movement. y = -speed;
       }
       
       // Collisionams tikrint uzsisaugom senas reiksmes
       float oldX = getX();
       float oldY = getY();
       
       //Collisionam konvertint is pixeliu i tiles
       float tileWidth = collisionLayer.getTileWidth();
       float tileHeight = collisionLayer.getTileHeight();
       
       // Ar collidinam ant X ir Y
       boolean collideX = false;
       boolean collideY = false;
       
       // Nustato, kad gali judėti
       // Judejimas ant X
       setX(getX() + movement.x * deltaTime);
       
       // Judam i kaire
       if (movement.x < 0){
           // Einam i kaire ir tikrinam
           // Virsuje viena
              collideX = collisionLayer.getCell( (int)(getX() / tileWidth), (int)((getY() 
                   + getHeight()) / tileHeight)).getTile().getProperties().containsKey("blocked");
           // Viduryje tris |= or
              if (!collideX)
              collideX = collisionLayer.getCell( (int)(getX() / tileWidth), (int)((getY() 
                   + getHeight() /2 ) / tileHeight)).getTile().getProperties().containsKey("blocked");
           //Apacioje viena 
              if (!collideX)
              collideX = collisionLayer.getCell( (int)(getX() / tileWidth), (int)((getY() 
                   ) / tileHeight)).getTile().getProperties().containsKey("blocked");
              
       // Judam i desine
       // virsus
       } else if (movement.x > 0){
            collideX = collisionLayer.getCell((int)((getX()+ getWidth()) / tileWidth), (int)((getY()
                    + getHeight())/tileHeight)).getTile().getProperties().containsKey("blocked");
       // vidurys
            if (!collideX){
                collideX = collisionLayer.getCell((int)((getX()+ getWidth()/2) / tileWidth), (int)((getY()
                    + getHeight())/tileHeight)).getTile().getProperties().containsKey("blocked");
            }
        // apacia  
            if (!collideX){
            collideX = collisionLayer.getCell((int)((getX()+ getWidth()) / tileWidth), (int)((getY()
                    )/tileHeight)).getTile().getProperties().containsKey("blocked");
            }
       }
       
       // Reaktinu i Y collisioną
       if (collideX){
           setX(oldX);
           movement.x = 0;
       }
       
       
       // Judejimas ant Y
       setY(getY() + movement.y * deltaTime);
       
       if (movement.y < 0){
           // Jei judam zemyn
           // kaire
                collideY = collisionLayer.getCell((int)((getX()) / tileWidth),
                        (int)((getY())/tileHeight))
                        .getTile().getProperties().containsKey("blocked");
           // vidurys
                if (!collideY)
                collideY = collisionLayer.getCell((int)((getX()+ getWidth()/2) / tileWidth),
                        (int)((getY())/tileHeight))
                        .getTile().getProperties().containsKey("blocked");
           // desine
                if (!collideY)
                collideY = collisionLayer.getCell((int)((getX()+ getWidth()) / tileWidth),
                        (int)((getY())/tileHeight))
                        .getTile().getProperties().containsKey("blocked");
           
       } else if (movement.y > 0){
           // Sokinejant
           // kaire
                collideY = collisionLayer.getCell((int)((getX()) / tileWidth),
                        (int)((getY()+ getHeight())/tileHeight))
                        .getTile().getProperties().containsKey("blocked");
           // vidurys
           if (!collideY)
             collideY = collisionLayer.getCell((int)((getX() + getWidth() / 2)/ tileWidth),
                     (int)((getY() + getHeight())/tileHeight))
                     .getTile().getProperties().containsKey("blocked");
           // desine
           if (!collideY)
             collideY = collisionLayer.getCell((int)(getX() + getWidth()),
                     (int)((getY()+ getHeight())/tileHeight))
                     .getTile().getProperties().containsKey("blocked");
       }
       
       
       // Reaktinu i Y collisioną
       if (collideY){
           setX(oldY);
           movement.y = 0;
       }
    }
    
    // GETTERIAI SETTERIAI
    
    public Vector2 getMovement() {
        return movement;
    }

    public void setMovement(Vector2 movement) {
        this.movement = movement;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getGravity() {
        return gravity;
    }

    public void setGravity(float gravity) {
        this.gravity = gravity;
    }

    public TiledMapTileLayer getCollisionLayer() {
        return collisionLayer;
    }

    public void setCollisionLayer(TiledMapTileLayer collisionLayer) {
        this.collisionLayer = collisionLayer;
    }
    
}
